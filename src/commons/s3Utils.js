const {
  S3Client,
  GetObjectCommand,
  PutObjectCommand,
  DeleteObjectCommand,
} = require("@aws-sdk/client-s3");
const { getSignedUrl } = require("@aws-sdk/s3-request-presigner");
const mime = require("mime-types");

const bucketName = process.env.AWS_BUCKET_NAME;
const region = process.env.AWS_BUCKET_REGION;
const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;

const s3Client = new S3Client({
  region,
  credentials: {
    accessKeyId,
    secretAccessKey,
  },
});

const uploadFile = async (file) => {
  const fileName = `${Date.now()}.${mime.extension(file.mimetype)}`;
  await s3Client.send(
    new PutObjectCommand({
      Bucket: bucketName,
      Body: file.buffer,
      Key: fileName,
    })
  );
  return fileName;
};

const getObjectSignedUrl = async (fileName) => {
  return await getSignedUrl(
    s3Client,
    new GetObjectCommand({
      Bucket: bucketName,
      Key: fileName,
    }),
    { expiresIn: 1800 } //seconds
  );
};

const deleteFile = (fileName) => {
  const deleteParams = {
    Bucket: bucketName,
    Key: fileName,
  };

  return s3Client.send(new DeleteObjectCommand(deleteParams));
};

module.exports = {
  uploadFile,
  getObjectSignedUrl,
  deleteFile,
};
