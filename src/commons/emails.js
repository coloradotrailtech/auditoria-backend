const { getActivationTemplate, getNewOrderTemplate, sendEmail } = require("../config/mail.config");

const sendActivationMail = async (razonSocial, user) => {
  try {
    const emailTemplate = getActivationTemplate(razonSocial, user._id);
    await sendEmail(user.email, "Activación de su cuenta en Auditoria de Riesgos", emailTemplate);
  } catch (e) {
    console.log(e);
    return res.json({
      success: false,
      msg: "Error al enviar email",
    });
  }
};

const sendNewOrdenMail = async (usuario, razonSocial, sucursal, fecha_inicio, fecha_fin) => {
  try {
    const emailTemplate = getNewOrderTemplate(razonSocial, sucursal, fecha_inicio);
    await sendEmail(usuario.email, "Nueva orden de servicio de Auditoria de Riesgos", emailTemplate);
  } catch (e) {
    console.log(e);
    return res.json({
      success: false,
      msg: "Error al enviar email",
    });
  }
};

const sendNewCapacitacionMail = async (usuario, razonSocial, sucursal, fecha_inicio, fecha_fin) => {
  try {
    const emailTemplate = getNewCapacitacionTemplate(razonSocial, sucursal, fecha_inicio);
    await sendEmail(usuario.email, "Nueva capacitacion de Auditoria de Riesgos", emailTemplate);
  } catch (e) {
    console.log(e);
    return res.json({
      success: false,
      msg: "Error al enviar email",
    });
  }
};

module.exports = {
  sendActivationMail,
  sendNewOrdenMail,
  sendNewCapacitacionMail
};
