const nodemailer = require("nodemailer");

const mail = {
  user: process.env.EMAIL_ADDRESS,
  pass: process.env.EMAIL_PASS,
};

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: mail.user,
    pass: mail.pass,
  },
});

const sendEmail = async (email, subject, html) => {
  try {
    await transporter.sendMail({
      from: "Auditoria de Riesgos <info@auditoriaderiesgos.com>", // sender address
      to: email,
      subject: subject, // Subject line
      path: __dirname + '/img/adr.png',
      text: "Estimado cliente, hermos enviado un enlace para que finalice con la activación de su cuenta debajo", // plain text body
      html,
    });
  } catch (error) {
    console.log("Algo fue mal con el envio de el email", error);
  }
};

/***************** 
 * TEMPLATES  
 * ***************/
const getActivationTemplate = (razonSocial, userId) => {
  return `<head>         
  </head>
  <div id="email___content">
      <h2>Hola estimado representante de <u>${razonSocial}</u></h2>
      <p>Para confirmar tu cuenta, haz click en el boton</p>
      <a 
        href="${process.env.CONFIRMATION_URL}/activation/${userId}" 
        style="background-color: #4CAF50; border-radious: 5px; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px;">
      Confirmar Cuenta
      </a>
  </div>`;
};

const getNewOrderTemplate = (razonSocial, sucursal, fecha_inicio) => {
  return `<head>         
  </head>
  <div id="email___content">
      <h2>Se ha generado una orden de servicio para desde Auditoria de Riesgos a <u>${razonSocial}</u></h2>
      <p>La presente tiene fecha de inicio en la sucursal ubicada en ${sucursal.direccion} el ${fecha_inicio}</p>
  </div>`;
};

module.exports = {
  sendEmail,
  getActivationTemplate,
  getNewOrderTemplate
};
