const { Schema, model } = require("mongoose");

const LocalidadSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true
    },
    provincia_id: {
      type: String,
      required: true
    },
    codigoPostal: {
      type: String,
      required: true,
      unique: true
    },
    pais_id: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = model("localidades", LocalidadSchema); // atamos el schema a la colección de documentos de mongodb
