const { Schema, model } = require("mongoose");

const ProvinciaSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
    },
    pais_id: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("provincias", ProvinciaSchema); // atamos el schema a la colección de documentos de mongodb
