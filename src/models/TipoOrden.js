const { Schema, model } = require("mongoose");

const TipoOrdenSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
      unique: true,
    },
    capacitacion: {
      type: Boolean,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("tiposOrdenes", TipoOrdenSchema); // atamos el schema a la colección de documentos de mongodb
