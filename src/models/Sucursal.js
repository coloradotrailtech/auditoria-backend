const { Schema, model } = require("mongoose");

const SucursalSchema = new Schema(
  {
    identificacion: {
      type: String,
    },
    direccion: {
      type: String,
    },
    telefono: {
      type: String,
    },
    cliente_id: {
      type: String,
      required: true,
    },
    localidad_id: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("sucursales", SucursalSchema); // atamos el schema a la colección de documentos de mongodb
