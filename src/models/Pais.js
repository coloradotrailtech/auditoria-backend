const { Schema, model } = require("mongoose");

const PaisSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("paises", PaisSchema); // atamos el schema a la colección de documentos de mongodb
