const { Schema, model } = require("mongoose");

const ClienteSchema = new Schema(
  {
    nombreFantasia: {
      type: String,
      required: true,
    },
    razonSocial: {
      type: String,
      required: true,
    },
    cuit: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: false,
      unique: true
    },
    telefono: {
      type: String,
    },
    pais_id: {
      type: String,
      required: true,
    },
    rubro_id: {
      type: String,
      required: true,
    },
    /*
    user_id: {
      type: String,
      required: true,
    },
    */
    categoriaAfip_id: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: false,
    },
    active: {
      type: Boolean,
      //required: true,
    }
  },
  {
    timestamps: true,
  }
);

module.exports = model("clientes", ClienteSchema); // atamos el schema a la colección de documentos de mongodb
