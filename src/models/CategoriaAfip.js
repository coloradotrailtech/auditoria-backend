const { Schema, model } = require("mongoose");

const CategoriaAfipSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("categoriasAfips", CategoriaAfipSchema); // atamos el schema a la colección de documentos de mongodb
