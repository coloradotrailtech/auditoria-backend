const { Schema, model } = require("mongoose");
const bcrypt = require('bcrypt');

const UserSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: false,
    },
    cliente_id: {
      type: String,
      required: false,
      default: null,
    },
    status: {
      type: String,
      required: true,
      default: "UNVERIFIED",
    },
    admin: {
      type: Boolean,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

UserSchema.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.statics.comparePassword = async (password, receivedPassword) => {
  return await bcrypt.compare(password, receivedPassword)
}

module.exports = model("users", UserSchema); // atamos el schema a la colección de documentos de mongodb