const { Schema, model } = require("mongoose");

const OrdenSchema = new Schema(
  {
    sucursal_id: {
      type: String,
      required: true,
    },

    tipoOrden_id: {
      type: String,
      required: true,
    },

    fechainicio: {
      type: String,
      required: true,
    },

    numeroorden: {
      type: Number,
      required: true,
    },

    fechafin: {
      type: String,
      required: false,
    },

    fechavencimiento: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("ordenes", OrdenSchema); // atamos el schema a la colección de documentos de mongodb
