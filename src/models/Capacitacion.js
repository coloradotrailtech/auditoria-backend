const { Schema, model } = require("mongoose");

const CapacitacionSchema = new Schema(
  {
    ispresencial: {
      type: Boolean,
      required: false,
    },

    horainicio: {
      type: String,
      required: true,
    },

    horafin: {
      type: String,
      required: true,
    },

    orden_id: {
      type: String,
      required: true,
    },

    planilla: {
      type: String,
      required: false,
    },

    asistentes: [
      {
        apellido: {
          type: String,
          required: true,
        },
        nombre: {
          type: String,
          required: true,
        },
        documento: {
          type: Number,
          required: true,
        },
      },
    ],
    detalles: {
      type: String,
    }
  },

  {
    timestamps: true,
  }
);

module.exports = model("capacitaciones", CapacitacionSchema); // atamos el schema a la colección de documentos de mongodb
