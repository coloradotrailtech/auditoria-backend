const { Schema, model } = require("mongoose");

const NotificacionSchema = new Schema(
  {
    user_id: {
      type: String,
      required: true,
      unique: false,
    },
    textoDescriptivo: {
        type: String,
        required: false,
        unique: false,
    },
    leida: {
      type: Boolean,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("notificaciones", NotificacionSchema); // atamos el schema a la colección de documentos de mongodb