const { Schema, model } = require("mongoose");

const RubroSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
      unique: true,
    },
    controlContratista: {
      type: Boolean,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("rubros", RubroSchema); // atamos el schema a la colección de documentos de mongodb
