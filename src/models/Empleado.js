const { Schema, model } = require("mongoose");

const EmpleadoSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
    },
    
    apellido: {
      type: String,
      required: false,
    },

    documento: {
      type: String,
      required: false,
    },

    sucursal_id: {
      type: String,
      required: true,
    },

    cliente_id: {
      type: String,
      required: false,
    },
    documento: {
      type: String,
      required: false,
    },
    fechadeingreso: {
      type: String,
      required: false,
    },
    /*
    profesion: {
      type: String,
      required: false,
    },
    */
    tipo: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("empleados", EmpleadoSchema); // atamos el schema a la colección de documentos de mongodb
