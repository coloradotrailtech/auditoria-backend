const multer = require("multer");
const path = require("path");
const uuid = require("uuid/v4");

// Settings
const storage = multer.diskStorage({
  destination: "./src/public/images/clientes",
  filename: (req, file, cb) => {
    cb(null, uuid() + path.extname(file.originalname));
  },
});

const multerIndex = {};

multerIndex.clientes = multer({
  storage,
  limits: { fileSize: 10000000 },
  fileFilter: function (req, file, cb) {
    var filetypes = /jpeg|jpg|png/;
    var mimetype = filetypes.test(file.mimetype);
    var extname = filetypes.test(path.extname(file.originalname).toLowerCase());

    if (mimetype && extname) {
      return cb(null, true);
    }
    cb(
      "Error: File upload only supports the following filetypes - " + filetypes
    );
  },
});

module.exports = multerIndex;
