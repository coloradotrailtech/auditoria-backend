const User = require("../models/User");
const jwt = require("jsonwebtoken");

const verifyToken = async (req, res, next) => {
  let token = req.headers["x-access-token"];
  console.log(token,"token")
  if (!token) return res.status(403).json({ message: "No token provided" });

  try {
    const decoded = jwt.verify(token, process.env.SECRET);
    const user = await User.findById(decoded.id, { password: 0 });
    req.user = user
    if (!user) return res.status(404).json({ message: "No user found" });
    next();
  } catch (error) {
    console.log(error)
    return res.status(401).json({ message: "Unauthorized!" });
  }
};

const verifyAdminToken = async (req, res, next) => {
  try {
    const decoded = jwt.verify(req.headers["x-access-token"], process.env.SECRET);
    const user = await User.findById(decoded.id, { password: 0 });
    //verify if user is a client
    if (user.cliente_id) {
      return(res.status(401).json({ message: "Unauthorized" }));
    } else{ 
      next() 
    }
  } catch (error) {
    return res.status(401).json({ message: "Unauthorized!" });
  }
}

module.exports = {
  verifyToken, verifyAdminToken
};
