const clientesCtrl = {};

const fs = require("fs-extra");
const path = require("path");
const { imagesClientes } = require("../constants/paths");
const Cliente = require("../models/Cliente");
const Sucursal = require("../models/Sucursal");
const CategoriaAfip = require("../models/CategoriaAfip");
const Pais = require("../models/Pais");
const Rubro = require("../models/Rubro");
const User = require("../models/User");
const { sendActivationMail } = require("../commons/emails");
const {
  uploadFile,
  deleteFile,
  getObjectSignedUrl,
} = require("../commons/s3Utils");

clientesCtrl.getClientes = async (req, res) => {
  try {
    const clientes_db = await Cliente.find();
    const clientes = await Promise.all(
      clientes_db.map(async (item) => {
        const linktofile = item.image
          ? await getObjectSignedUrl(item.image)
          : null;
        const sucursales = await Sucursal.find({ cliente_id: item._id });
        const categoria = await CategoriaAfip.findById(item.categoriaAfip_id);
        const pais = await Pais.findById(item.pais_id);
        const rubro = await Rubro.findById(item.rubro_id);
        return {
          ...item._doc,
          filename: item.image,
          image: null,
          linktofile,
          categoria,
          pais,
          rubro,
          sucursales,
        };
      })
    );

    res.json(clientes);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

clientesCtrl.createCliente = async (req, res) => {
  try {
    const {
      nombreFantasia,
      razonSocial,
      telefono,
      email,
      cuit,
      pais_id,
      rubro_id,
      categoriaAfip_id,
    } = req.body;

    const cliente_duplicado = await Cliente.find({ email: req.body.email });
    console.log("duplicado_encontrado", cliente_duplicado);

    //si no hay considencias el array viene vacio (no es null, es vacio)
    if (cliente_duplicado.length == 0) {
      /**
       * Upload File
       */
      const image = req.file ? await uploadFile(req.file) : null;

      const newCliente = new Cliente({
        nombreFantasia,
        razonSocial,
        telefono,
        cuit,
        email,
        pais_id,
        rubro_id,
        categoriaAfip_id,
        image,
        active: true,
      });
      await newCliente.save();

      /**
       * Creamos un usuario para un nuevo cliente
       */
      const newUser = new User({ email, cliente_id: newCliente._id });
      await newUser.save();

      /**
       * Enviamos el email al cliente
       */
      sendActivationMail(razonSocial, newUser);
      res.json({ _id: newCliente._id, image: newCliente.image });
    } else {
      //cliente duplicado
      const mensajeClienteDuplicado = "cliente duplicado";
      console.log(mensajeClienteDuplicado);
      throw new Error(mensajeClienteDuplicado); //throw exception
    }
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

clientesCtrl.getCliente = async (req, res) => {
  try {
    const cliente = await Cliente.findById(req.params.id);
    res.json({ cliente });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

clientesCtrl.deleteCliente = async (req, res) => {
  try {
    const { image } = await Cliente.findByIdAndDelete(req.params.id);
    if (image) await deleteFile(image);

    res.json("Cliente Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

clientesCtrl.updateCliente = async (req, res) => {
  try {
    const {
      nombreFantasia,
      razonSocial,
      telefono,
      cuit,
      email,
      pais_id,
      rubro_id,
      categoriaAfip_id,
      oldfilename,
    } = req.body;

    let update_cliente = {};

    update_cliente =
      nombreFantasia !== undefined
        ? { ...update_cliente, nombreFantasia }
        : update_cliente;

    update_cliente =
      razonSocial !== undefined
        ? { ...update_cliente, razonSocial }
        : update_cliente;

    update_cliente =
      telefono !== undefined ? { ...update_cliente, telefono } : update_cliente;

    update_cliente =
      cuit !== undefined ? { ...update_cliente, cuit } : update_cliente;

    update_cliente =
      email !== undefined ? { ...update_cliente, email } : update_cliente;

    update_cliente =
      pais_id !== undefined ? { ...update_cliente, pais_id } : update_cliente;

    update_cliente =
      rubro_id !== undefined ? { ...update_cliente, rubro_id } : update_cliente;

    update_cliente =
      categoriaAfip_id !== undefined
        ? { ...update_cliente, categoriaAfip_id }
        : update_cliente;

    /**
     * Update File
     */
    const image = req.file ? await uploadFile(req.file) : oldfilename;
    if (image != oldfilename) await deleteFile(oldfilename);

    update_cliente = {
      ...update_cliente,
      image: image || null,
    };

    await Cliente.findByIdAndUpdate(req.params.id, update_cliente);
    res.json("Cliente Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

clientesCtrl.deactivateCliente = async (req, res) => {
  try {
    const { active } = req.body;
    let datos_nuevos = {};
    datos_nuevos =
      active !== undefined ? { ...datos_nuevos, active } : datos_nuevos;

    await Cliente.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("Cliente Deactivated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

const createUser = async (req, res) => {
  try {
    // obtener email
    //verificar que usuario existe
    //generar codigo
    //crear un nuevo usuario
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = clientesCtrl;
