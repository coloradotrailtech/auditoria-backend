const localidadesCtrl = {};

const Localidad = require("../models/Localidad");
const Provincia = require("../models/Provincia");
const Pais = require("../models/Pais");

localidadesCtrl.getLocalidades = async (req, res) => {
  try {
    const localidades_db = await Localidad.find();
    let localidades = [];
    for (let index = 0; index < localidades_db.length; index++) {
      const localidad_db = localidades_db[index];
      const provincia = await Provincia.findById(localidad_db.provincia_id);
      const pais = await Pais.findById(localidad_db.pais_id);
      let localidad =
        provincia !== undefined
          ? { ...localidad_db._doc, provincia }
          : localidad_db;
        pais !== undefined
          ? { ...localidad_db.doc, pais }
          : localidad_db;  
      localidades.push(localidad);
    }
    res.json(localidades);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

localidadesCtrl.createLocalidad = async (req, res) => {
  try {
    const { nombre, provincia_id, codigoPostal, pais_id } = req.body;
    const newLocalidad = new Localidad({
      nombre,
      provincia_id,
      codigoPostal,
      pais_id,
    });
    await newLocalidad.save();
    res.json({ _id: newLocalidad._id });
  } catch (e) {
    res.json(e.message);
  }
};

localidadesCtrl.getLocalidad = async (req, res) => {
  try {
    const localidad = await Localidad.findById(req.params.id);
    res.json({ localidad });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

localidadesCtrl.deleteLocalidad = async (req, res) => {
  try {
    await Localidad.findByIdAndDelete(req.params.id);
    res.json("Localidad Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

localidadesCtrl.updateLocalidad = async (req, res) => {
  try {
    const { nombre, provincia_id, codigoPostal, pais_id } = req.body;
    datos_nuevos = {};
    datos_nuevos =
      nombre !== undefined 
        ? { ...datos_nuevos, nombre } 
        : datos_nuevos;
    datos_nuevos =
      provincia_id !== undefined
        ? { ...datos_nuevos, provincia_id }
        : datos_nuevos;
    datos_nuevos =
      codigoPostal !== undefined
        ? { ...datos_nuevos, codigoPostal }
        : datos_nuevos;
    datos_nuevos = 
      pais_id !== undefined
        ? { ...datos_nuevos, pais_id }
        : datos_nuevos;
    await Localidad.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("Localidad Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = localidadesCtrl;
