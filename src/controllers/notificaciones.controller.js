const notificacionesCtrl = {};

const Notificacion = require("../models/Notificacion");

notificacionesCtrl.getNotificaciones = async (req, res) => {
    try {
        const notificaciones_db = await Notificacion.find({ user_id: req.user._id })
        res.json(notificaciones_db.map(item=> item._doc));
    } catch (e) {
        console.log(e);
        res.json(e.message);
    }
};

notificacionesCtrl.createNotificacion = async (req, res) => {
    try {
        const {
            user_id,
            textoDescriptivo,
            leida,
        } = req.body;

        const newNotificacion = new Notificacion({
            user_id,
            textoDescriptivo,
            leida,
        });
        await newNotificacion.save();
        res.json({ _id: newNotificacion._id, mensaje: "notificación creada exitosamente"});
    }   catch (e) {
        console.log(e);
        res.json(e.message);
    }
};

module.exports = notificacionesCtrl;