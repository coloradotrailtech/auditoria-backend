const userCtrl = {};

const User = require("../models/User");
const { getTemplate, sendEmail } = require("../config/mail.config");
const { getToken, getTokenData } = require("../config/jwt.config");
const { v4: uuidv4 } = require("uuid");

userCtrl.getUsers = async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    res.status(400).json({
      error: err,
    });
  }
};

userCtrl.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.json({ email: user.email, status: user.status });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

/** Este medodo envia el email para activar la cuenta (paso 1) **/
/* userCtrl.signUp = async (req, res) => {
  console.log("se entro a singUp");
  try {
    const { razonSocial, email } = req.body;
    //generar codigo (password)
    const code = uuidv4();
    const token = getToken(email, code);
    const emailTemplate = getTemplate(razonSocial, token);
    await sendEmail(email, "Este es un email de prueba", emailTemplate);
  } catch (e) {
    console.log(e);
    return res.json({
      success: false,
      msg: "Error al enviar email",
    });
  }
}; */

/** Este otro medodo crea la cuenta de usuaraio efectivamente (paso 2) **/
userCtrl.createUser = async (req, res) => {
  console.log("se entro a createUser");
  try {
    //try obtener TOKEN
    const { token } = req.params;
    //verificar data
    const data = await getTokenData(token);

    if (data === null) {
      return res.json({
        success: false,
        msg: "Error al obtener data",
      });
    }
    const { email, password } = req.body;
    let user = (await User.findOne({ email })) || null;
    if (user != null) {
      return res.json({
        success: false,
        msg: "Usuario ya existente",
      });
    }

    const newUser = new User({ email, password, cliente_id });
    newUser.password = await User.encryptPassword(password); // encrypting password
    await newUser.save();
    res.json({ _id: newUser._id, cliente_id: newCliente._id });
  } catch (e) {
    console.log(e);
    return res.json({
      success: false,
      msg: "Error al registrar usuario",
    });
  }
};

userCtrl.updateUser = async (req, res) => {
  try {
    const { password, status } = req.body;
    datos_nuevos = {};
    datos_nuevos =
      status !== undefined ? { ...datos_nuevos, status } : datos_nuevos;

    if (password !== undefined) {
      console.log(`pass: ${password}`);
      encryptPassword = await User.encryptPassword(password); // encrypting password
      datos_nuevos = { ...datos_nuevos, password: encryptPassword };
      console.log(datos_nuevos);
    }

    await User.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("User Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

userCtrl.deleteUser = async (req, res) => {
  const { id } = req.params;
  await User.findByIdAndDelete(id);
  res.json("User deleted");
};

module.exports = userCtrl;
