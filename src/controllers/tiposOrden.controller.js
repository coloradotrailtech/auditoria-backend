const tiposOrdenCtrl = {};

const TipoOrden = require("../models/TipoOrden");

tiposOrdenCtrl.getTiposOrden = async (req, res) => {
  const tiposOrden = await TipoOrden.find();
  res.json(tiposOrden);
};

tiposOrdenCtrl.getTipoOrden = async (req, res) => {
  try {
    const tipoOrden = await TipoOrden.findById(req.params.id);
    res.json({ tipoOrden });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

tiposOrdenCtrl.deleteTipoOrden = async (req, res) => {
  try {
    const tipoOrden = await TipoOrden.findByIdAndDelete(req.params.id);
    res.json("TipoOrden Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

tiposOrdenCtrl.updateTipoOrden = async (req, res) => {
  try {
    const {
      nombre, 
      capacitacion 
    } = req.body;

    datos_nuevos = {};

    datos_nuevos =
      nombre !== undefined
       ? { ...datos_nuevos, nombre } 
       : datos_nuevos;

    datos_nuevos =
      capacitacion !== undefined
        ? { ...datos_nuevos, capacitacion }
        : datos_nuevos;

    await TipoOrden.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("TipoOrden Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

tiposOrdenCtrl.createTipoOrden = async (req, res) => {
  try {
    const {
      nombre,
      capacitacion,
    } = req.body;

    const newTipoOrden = new TipoOrden({
      nombre,
      capacitacion,
    });
    await newTipoOrden.save();
    res.json({ _id: newTipoOrden._id, mensaje: "exito" });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = tiposOrdenCtrl;
