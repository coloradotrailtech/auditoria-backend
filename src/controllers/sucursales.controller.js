const sucursalesCtrl = {};

const Sucursal = require("../models/Sucursal");
const Localidad = require("../models/Localidad");
const Cliente = require("../models/Cliente");
const Orden = require("../models/Orden");

sucursalesCtrl.getSucursales = async (req, res) => {
  try {
    const params = req.user.cliente_id ? {cliente_id : req.user.cliente_id} : {}
    const sucursales_db = await Sucursal.find(params);
    let sucursales = [];
    for (let index = 0; index < sucursales_db.length; index++) {
      const sucursal = sucursales_db[index];
      const localidad = await Localidad.findById(sucursal.localidad_id);
      const cliente = await Cliente.findById(sucursal.cliente_id);
      const cantidad_ordenes = await Orden.countDocuments({sucursal_id: sucursal._id});  //numero total de ordenes (de acá filtrar por sucursal)
      sucursales.push({
        ...sucursal._doc,
        localidad,
        cliente,
        cantidad_ordenes,
      });
    }
    res.json(sucursales);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

sucursalesCtrl.createSucursal = async (req, res) => {
  try {
    const {
      identificacion,
      direccion,
      telefono,
      cliente_id,
      localidad_id,
      latitud,
      longitud,
    } = req.body;
    const newSucursal = new Sucursal({
      identificacion,
      direccion,
      telefono,
      cliente_id,
      localidad_id,
      latitud,
      longitud,
    });
    await newSucursal.save();
    res.json({ _id: newSucursal._id });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

sucursalesCtrl.getSucursal = async (req, res) => {
  try {    
    const sucursal = await Sucursal.findById(req.params.id);
    res.json({ sucursal });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

sucursalesCtrl.deleteSucursal = async (req, res) => {
  try {
    await Sucursal.findByIdAndDelete(req.params.id);
    res.json("Sucursal Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

sucursalesCtrl.updateSucursal = async (req, res) => {
  try {
    const {
      direccion,
      telefono,
      cliente_id,
      localidad_id,
    } = req.body;
    
    datos_nuevos = {};

    datos_nuevos =
      direccion !== undefined
      ? { ...datos_nuevos, direccion }
      : datos_nuevos;

    datos_nuevos =
      telefono !== undefined
      ? { ...datos_nuevos, telefono }
      : datos_nuevos;

    datos_nuevos =
      cliente_id !== undefined
      ? { ...datos_nuevos, cliente_id }
      : datos_nuevos;

    datos_nuevos =
      localidad_id !== undefined
      ? { ...datos_nuevos, localidad_id }
      : datos_nuevos;    
    await Sucursal.findByIdAndUpdate(req.params.id, datos_nuevos);  
    res.json("Sucursal Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = sucursalesCtrl;
