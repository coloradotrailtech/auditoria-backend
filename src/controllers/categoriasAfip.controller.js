const categoriasCtrl = {};

const CategoriaAfip = require("../models/CategoriaAfip");

categoriasCtrl.getCategoriasAfip = async (req, res) => {
  try {
    const categorias = await CategoriaAfip.find();
    res.json(categorias);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

categoriasCtrl.createCategoriaAfip = async (req, res) => {
  try {
    const { nombre } = req.body;
    const newCategoriaAfip = new CategoriaAfip({
      nombre,
    });
    await newCategoriaAfip.save();
    res.json({ _id: newCategoriaAfip._id });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

categoriasCtrl.getCategoriaAfip = async (req, res) => {
  try {
    const categoria = await CategoriaAfip.findById(req.params.id);
    res.json({ categoria });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

categoriasCtrl.deleteCategoriaAfip = async (req, res) => {
  try {
    await CategoriaAfip.findByIdAndDelete(req.params.id);
    res.json("CategoriaAfip Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

categoriasCtrl.updateCategoriaAfip = async (req, res) => {
  try {
    const { nombre } = req.body;
    await CategoriaAfip.findByIdAndUpdate(req.params.id, { nombre });
    res.json("CategoriaAfip Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = categoriasCtrl;
