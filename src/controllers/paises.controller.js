const paisesCtrl = {};

const Pais = require("../models/Pais");

paisesCtrl.getPaises = async (req, res) => {
  const paises = await Pais.find();
  res.json(paises);
};

paisesCtrl.getPais = async (req, res) => {
  const pais = await Pais.findById(req.params.id);
  res.json(pais);
};

module.exports = paisesCtrl;
