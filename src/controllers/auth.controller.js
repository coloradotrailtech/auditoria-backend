const authCtrl = {};

const User = require("../models/User");
const Cliente = require("../models/Cliente");

const jwt = require("jsonwebtoken");
const { getObjectSignedUrl } = require("../commons/s3Utils");

authCtrl.signIn = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userFound = await User.findOne({ email });
    let cliente = null;

    if (!userFound) return res.status(400).json({ msg: "email not found" });
    if (userFound.cliente_id)
      cliente = await Cliente.findById(userFound.cliente_id);

    const matchPassword = await User.comparePassword(
      password,
      userFound.password
    );

    if (!matchPassword)
      return res.status(401).json({ msg: "password not matching" });

    const token = jwt.sign({ id: userFound._id }, process.env.SECRET, {
      expiresIn: 86400, // 24 hours
    });

    if (cliente) {
      const linktofile = cliente.image
        ? await getObjectSignedUrl(cliente.image)
        : null;

      cliente = {
        ...cliente._doc,
        filename: cliente.image,
        image: null,
        linktofile,
      };
    }

    //limpiar password de userFound
    const { status } = userFound;
    return res.json({
      token,
      user: {
        cliente,
        status,
      },
    });
  } catch (e) {
    console.log(e);
    return res.json({
      success: false,
      msg: "error",
      e,
    });
  }
};

authCtrl.verifyUser = async (req, res) => {
  try {
    const userFound = await User.findById(req.params.id);
    if (!userFound || userFound.status !== "UNVERIFIED")
      return res.status(404).json({ token: "" });
    const token = jwt.sign({ id: userFound._id }, process.env.SECRET, {
      expiresIn: 900, // 15 minutes
    });
    res.json({ token });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = authCtrl;
