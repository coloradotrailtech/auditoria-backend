const empleadosCtrl = {};

const Empleado = require("../models/Empleado");
const Cliente = require("../models/Cliente");
const Sucursal = require("../models/Sucursal");

empleadosCtrl.getEmpleados = async (req, res) => {
  try {
    const params = req.user.cliente_id
      ? { cliente_id: req.user.cliente_id }
      : {};
    const empleados_db = await Empleado.find(params);
    const empleados = await Promise.all(
      empleados_db.map(async (item) => {
        const cliente = await Cliente.findById(item.cliente_id);
        const sucursal = await Sucursal.findById(item.sucursal_id);
        return {
          ...item._doc,
          cliente,
          sucursal,
        };
      })
    );
    res.json(empleados);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

empleadosCtrl.getEmpleadosCliente = async (req, res) => {
  try {
    const empleados_db = await Empleado.find({ cliente_id: req.params.id });
    const empleados = empleados_db.map(({ nombre, apellido, documento }) => {
      return { nombre, apellido, documento };
    });
    res.json(empleados);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

empleadosCtrl.getEmpleado = async (req, res) => {
  try {
    const empleado = await Empleado.findById(req.params.id);
    res.json({ empleado });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

empleadosCtrl.deleteEmpleado = async (req, res) => {
  try {
    const empleado = await Empleado.findByIdAndDelete(req.params.id);
    res.json("Empleado Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

empleadosCtrl.updateEmpleado = async (req, res) => {
  try {
    const {
      nombre,
      apellido,
      cliente_id,
      documento,
      fechadeingreso,
      tipo,
      sucursal_id,
    } = req.body;

    datos_nuevos = {};

    datos_nuevos =
      nombre !== undefined ? { ...datos_nuevos, nombre } : datos_nuevos;

    datos_nuevos =
      apellido !== undefined ? { ...datos_nuevos, apellido } : datos_nuevos;

    datos_nuevos =
      documento !== undefined ? { ...datos_nuevos, documento } : datos_nuevos;

    datos_nuevos =
      sucursal_id !== undefined
        ? { ...datos_nuevos, sucursal_id }
        : datos_nuevos;

    datos_nuevos =
      cliente_id !== undefined ? { ...datos_nuevos, cliente_id } : datos_nuevos;

    datos_nuevos =
      fechadeingreso !== undefined
        ? { ...datos_nuevos, fechadeingreso }
        : datos_nuevos;

    datos_nuevos =
      tipo !== undefined ? { ...datos_nuevos, tipo } : datos_nuevos;

    await Empleado.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("Empleado Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

empleadosCtrl.createEmpleado = async (req, res) => {
  try {
    const {
      nombre,
      apellido,
      fechadeingreso,
      tipo,
      documento,
      cliente_id,
      sucursal_id,
    } = req.body;

    const newEmpleado = new Empleado({
      nombre,
      apellido,
      fechadeingreso,
      tipo,
      documento,
      cliente_id,
      sucursal_id,
    });
    await newEmpleado.save();
    res.json({ _id: newEmpleado._id, mensaje: "exito" });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = empleadosCtrl;
