const rubrosCtrl = {};

const Rubro = require("../models/Rubro");

rubrosCtrl.getRubros = async (req, res) => {
  try {
    const rubros = await Rubro.find();
    res.json(rubros);
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

rubrosCtrl.createRubro = async (req, res) => {
  try {
    const { nombre, controlContratista } = req.body;
    const newRubro = new Rubro({
      nombre,
      controlContratista,
    });
    await newRubro.save();
    res.json({ _id: newRubro._id });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

rubrosCtrl.getRubro = async (req, res) => {
  try {
    const rubro = await Rubro.findById(req.params.id);
    res.json({ rubro });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

rubrosCtrl.deleteRubro = async (req, res) => {
  try {
    await Rubro.findByIdAndDelete(req.params.id);
    res.json("Rubro Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

rubrosCtrl.updateRubro = async (req, res) => {
  try {
    const {
      nombre, 
      controlContratista 
    } = req.body;

    datos_nuevos = {};

    datos_nuevos =
      nombre !== undefined 
      ? { ...datos_nuevos, nombre } 
      : datos_nuevos;

    datos_nuevos =
      controlContratista !== undefined
        ? { ...datos_nuevos, controlContratista }
        : datos_nuevos;

    await Rubro.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("Rubro Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = rubrosCtrl;
