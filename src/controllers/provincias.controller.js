const provinciasCtrl = {};

const Provincia = require("../models/Provincia");

provinciasCtrl.getProvincias = async (req, res) => {
  let provincias_db = await Provincia.find();
  res.json(provincias_db);
};

provinciasCtrl.getProvincia = async (req, res) => {
  const provincia = await Provincia.findById(req.params.id);
  res.json(provincia);
};

module.exports = provinciasCtrl;
