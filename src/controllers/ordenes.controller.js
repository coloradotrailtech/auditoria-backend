const ordenesCtrl = {};

const Orden = require("../models/Orden");
const Capacitacion = require("../models/Capacitacion");
const TipoOrden = require("../models/TipoOrden");
const Sucursal = require("../models/Sucursal");
const Cliente = require("../models/Cliente");
const User = require("../models/User");
const Localidad = require("../models/Localidad");
const { deleteFile, getObjectSignedUrl } = require("../commons/s3Utils");
const { sendNewOrdenMail } = require("../commons/emails");
const Notificacion = require("../models/Notificacion");

ordenesCtrl.getOrdenes = async (req, res) => {
  let params = {};

  if (req.user.cliente_id) {
    const sucursales_cliente = await Sucursal.find({
      cliente_id: req.user.cliente_id,
    });
    const sucursales_id = sucursales_cliente.map((item) => item._id);
    params = { sucursal_id: sucursales_id };
  }

  const ordenes_db = await Orden.find(params);
  const ordenes = await Promise.all(
    ordenes_db.map(async (item) => {
      const tipo_orden = await TipoOrden.findById(item.tipoOrden_id);
      const orden_sucursal = await Sucursal.findById(item.sucursal_id);
      const orden_localidad_sucursal = await Localidad.findById(
        orden_sucursal.localidad_id
      );
      const orden_cliente = await Cliente.findById(orden_sucursal.cliente_id);

      let orden_capacitacion = await Capacitacion.findOne({
        orden_id: item._id,
      });

      if (orden_capacitacion && orden_capacitacion.planilla) {
        const linktofile = await getObjectSignedUrl(
          orden_capacitacion.planilla
        );
        orden_capacitacion = {
          ...orden_capacitacion._doc,
          filename: orden_capacitacion.planilla,
          planilla: null,
          linktofile,
        };
      }

      //console.log(orden_capacitacion, "(2)")

      return {
        ...item._doc,
        capacitacion: orden_capacitacion,
        sucursal: {
          ...orden_sucursal._doc,
          cliente: orden_cliente,
          localidad: orden_localidad_sucursal,
        },
        tipoOrden: tipo_orden,
      };
    })
  );

  res.json(ordenes);
};

ordenesCtrl.createOrden = async (req, res) => {
  try {
    const {
      sucursal_id,
      tipoOrden_id,
      fechainicio,
      fechafin,
      fechavencimiento,
    } = req.body;

    //control de orden duplicada
    const orden_duplicado = await Orden.find({
      sucursal_id: req.body.sucursal_id,
      tipoOrden_id: req.body.tipoOrden_id,
      fechainicio: req.body.fechainicio,
    });
    console.log("duplicado_encontrado", orden_duplicado);
    if (orden_duplicado.length == 0) {

      const lastest_array = await Orden.find().sort({createdAt: -1}).limit(1); //devuelve un array
      const numeroorden =  lastest_array[0] ? lastest_array[0].numeroorden + 1 : 1;

      const newOrden = new Orden({
        sucursal_id,
        tipoOrden_id,
        fechainicio,
        fechafin,
        numeroorden,
        fechavencimiento,
      });
      await newOrden.save();

      /* Obtener el usuario relacionado a esa orden (para la notificacion) */
      const sucursal = await Sucursal.findById(sucursal_id);
      const usuario = await User.findOne({ cliente_id: sucursal.cliente_id });
      /*## Joaquin: ojo que acá arriba solo va a crear 1 Notificacion para 1 usuario...
      ## puede que necesitemos crear 1 notificacion por cada usuario con accesso a datos de esa sucursal
      */
      /** 1) Creamos Notificacion asociada al usuario que deberia verla */
      console.log(usuario, "usuario recibiendo notificacion");
      const notificacion = new Notificacion({
        user_id: usuario._id,
        textoDescriptivo: `Se ha creado una nueva orden para la sucursal ubicada en ${sucursal.direccion}`,
        leida: false,
      }).save();
      // 2) Enviamos un aviso de notificacion (orden creada) por mail al cliente/usuario
      const cliente = await Cliente.findById(sucursal.cliente_id);
      sendNewOrdenMail(usuario, cliente.razonSocial, sucursal, fechainicio, fechafin);
      res.json({ _id: newOrden._id, sucursal: sucursal, usuario: usuario });
    } else {
      //orden duplicado
      const mensajeOrdenDuplicado = "Orden duplicada";
      console.log(mensajeOrdenDuplicado);
      throw new Error(mensajeOrdenDuplicado);
    }
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

ordenesCtrl.getOrden = async (req, res) => {
  try {
    const orden = await Orden.findById(req.params.id);
    res.json({ orden });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

ordenesCtrl.deleteOrden = async (req, res) => {
  try {
    await Orden.findByIdAndDelete(req.params.id);
    const { planilla } = await Capacitacion.findOneAndDelete({
      orden_id: req.params.id,
    });
    if (planilla) await deleteFile(planilla);
    res.json("Orden Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

ordenesCtrl.updateOrden = async (req, res) => {
  try {
    const {
      sucursal_id,
      tipoOrden_id,
      fechainicio,
      fechafin,
      fechavencimiento,
      numeroorden,
    } = req.body;

    datos_nuevos = {};

    datos_nuevos =
      sucursal_id !== undefined
        ? { ...datos_nuevos, sucursal_id }
        : datos_nuevos;

    datos_nuevos =
      tipoOrden_id !== undefined
        ? { ...datos_nuevos, tipoOrden_id }
        : datos_nuevos;

    datos_nuevos =
      fechainicio !== undefined
        ? { ...datos_nuevos, fechainicio }
        : datos_nuevos;

    datos_nuevos =
      fechafin !== undefined ? { ...datos_nuevos, fechafin } : datos_nuevos;

    datos_nuevos =
      fechavencimiento !== undefined
        ? { ...datos_nuevos, fechavencimiento }
        : datos_nuevos;

    datos_nuevos =
      numeroorden !== undefined
        ? { ...datos_nuevos, numeroorden }
        : datos_nuevos;

    await Orden.findByIdAndUpdate(req.params.id, datos_nuevos);
    res.json("Orden Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = ordenesCtrl;
