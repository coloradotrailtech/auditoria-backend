const ordenesCtrl = {};

const Capacitacion = require("../models/Capacitacion");
const Empleado = require("../models/Empleado");
const Orden = require("../models/Orden");
const Sucursal = require("../models/Sucursal");
const Notificacion = require("../models/Notificacion");
const {
  uploadFile,
  getObjectSignedUrl,
  deleteFile,
} = require("../commons/s3Utils");

ordenesCtrl.getCapacitaciones = async (req, res) => {
  const capacitaciones_db = await Capacitacion.find();
  const capacitaciones = await Promise.all(
    capacitaciones_db.map(async (item) => {
      const planilla = item.planilla
        ? await getObjectSignedUrl(item.planilla)
        : null;
      return { ...item._doc, planilla };
    })
  );
  res.json(capacitaciones);
};

ordenesCtrl.createCapacitacion = async (req, res) => {
  try {
    const {
      ispresencial,
      horainicio,
      horafin,
      attendees,
      sucursal_id,
      fechainicio,
      fechafin,
      fechavencimiento,
      tipoOrden_id,
    } = req.body;

    /**
     * Upload File
     */
    const planilla = req.file ? await uploadFile(req.file) : null;

    /**
     * Set número orden
     */

     const lastest_array = await Orden.find().sort({createdAt: -1}).limit(1); //devuelve un array
     const numeroorden =  lastest_array[0] ? lastest_array[0].numeroorden + 1 : 1;

    /**
     * Create Orden
     */

    const newOrden = new Orden({
      sucursal_id,
      numeroorden,
      tipoOrden_id,
      fechainicio,
      fechafin,
      fechavencimiento,
    });
    await newOrden.save();

    /**
     * Create Capacitación
     */
    const orden_id = newOrden._id;

    const asistentes = JSON.parse(attendees) || [];

    const newCapacitacion = new Capacitacion({
      orden_id,
      ispresencial,
      horainicio,
      horafin,
      planilla,
      asistentes,
    });
    await newCapacitacion.save();

    //Notificacion asociada a usuario
    const sucursal = await Sucursal.findById(sucursal_id);
    const usuario = await User.findOne({ cliente_id: sucursal.cliente_id });
    const notificacion = new Notificacion({
      user_id: usuario._id,
      textoDescriptivo: `Se registro una capacitación para su sucursal ubicada en ${sucursal.direccion}`,
      leida: false,
    }).save();

    //Email avisando de Capacitacion Nueva
    const cliente = await Cliente.findById(sucursal.cliente_id);
    sendNewCapacitacionMail(usuario, cliente.razonSocial, sucursal, fechainicio, fechafin);

    /**
     * Create Empleados
     */

    asistentes.map(async ({ nombre, apellido, documento }) => {
      const query = await Empleado.findOne({
        nombre,
        apellido,
        documento,
        cliente_id: sucursal.cliente_id,
      });

      if (!query) {
        const newEmpleado = new Empleado({
          nombre,
          apellido,
          documento,
          sucursal_id,
          cliente_id: sucursal.cliente_id,
        });
        newEmpleado.save();
      }
    });

    res.json({ _id: newCapacitacion._id, mensaje: "exito" });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

ordenesCtrl.getCapacitacion = async (req, res) => {
  try {
    const orden = await Capacitacion.findById(req.params.id);
    res.json({ orden });
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

ordenesCtrl.deleteCapacitacion = async (req, res) => {
  try {
    const { planilla } = await Capacitacion.findByIdAndDelete(req.params.id);
    if (planilla) await deleteFile(planilla);
    res.json("Capacitación Deleted");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

ordenesCtrl.updateCapacitacion = async (req, res) => {
  try {
    const {
      orden_id,
      numeroorden,
      horainicio,
      horafin,
      attendees,
      ispresencial,
      tipoOrden_id,
      sucursal_id,
      cliente_id,
      fechainicio,
      fechafin,
      fechavencimiento,
      oldfilename,
    } = req.body;

    /**
     * Validaciones
     */

    let update_orden = {};
    let update_capacitacion = {};

    /**
     * Orden
     */
    update_orden =
      sucursal_id !== undefined
        ? { ...update_orden, sucursal_id }
        : update_orden;

    update_orden =
      numeroorden !== undefined
        ? { ...update_orden, numeroorden }
        : update_orden;

    update_orden =
      tipoOrden_id !== undefined
        ? { ...update_orden, tipoOrden_id }
        : update_orden;

    update_orden =
      fechainicio !== undefined
        ? { ...update_orden, fechainicio }
        : update_orden;
    update_orden =
      fechafin !== undefined ? { ...update_orden, fechafin } : update_orden;

    update_orden =
      fechavencimiento !== undefined
        ? { ...update_orden, fechavencimiento }
        : update_orden;

    /**
     * Capacitacion
     */
    update_capacitacion =
      horainicio !== undefined
        ? { ...update_capacitacion, horainicio }
        : update_capacitacion;

    update_capacitacion =
      horafin !== undefined
        ? { ...update_capacitacion, horafin }
        : update_capacitacion;

    /**
     * Update File
     */
    const planilla = req.file ? await uploadFile(req.file) : oldfilename;
    if (planilla != oldfilename) await deleteFile(oldfilename);

    update_capacitacion = {
      ...update_capacitacion,
      planilla: planilla || null,
    };

    const asistentes = JSON.parse(attendees) || [];

    update_capacitacion =
      attendees !== undefined
        ? {
            ...update_capacitacion,
            asistentes,
          }
        : update_capacitacion;

    update_capacitacion =
      ispresencial !== undefined
        ? { ...update_capacitacion, ispresencial }
        : update_capacitacion;

    /**
     * Actualización
     */
    await Orden.findByIdAndUpdate(orden_id, update_orden);
    await Capacitacion.findByIdAndUpdate(req.params.id, update_capacitacion);

    /**
     * Empleados
     */

    asistentes.map(async ({ nombre, apellido, documento }) => {
      const query = await Empleado.findOne({
        nombre,
        apellido,
        documento,
        cliente_id: cliente_id,
      });

      if (!query) {
        const newEmpleado = new Empleado({
          nombre,
          apellido,
          documento,
          sucursal_id,
          cliente_id: cliente_id,
        });
        newEmpleado.save();
      }
    });

    res.json("Capacitación Updated");
  } catch (e) {
    console.log(e);
    res.json(e.message);
  }
};

module.exports = ordenesCtrl;
