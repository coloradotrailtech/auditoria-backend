const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const path = require("path");
const app = express();

const { verifyToken, verifyAdminToken } = require("./middlewares/auth.jwt");
const { upload } = require("./config/multer.config");

// settings
app.set("port", process.env.PORT || 4000);

// middlewares
app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

// routes
app.use("/api/clientes", [verifyToken, upload.single("image")], require("./routes/clientes"));
app.use("/api/empleados", [verifyToken], require("./routes/empleados"));
app.use("/api/tiposOrden", [verifyAdminToken], require("./routes/tiposOrden"));
app.use("/api/ordenes", [verifyToken], require("./routes/ordenes"));
app.use("/api/notificaciones", [verifyToken], require("./routes/notificaciones"));
app.use(
  "/api/capacitaciones",
  [verifyToken, upload.single("planilla")],
  require("./routes/capacitaciones")
);
app.use("/api/paises", [verifyAdminToken], require("./routes/paises"));
app.use("/api/provincias", [verifyAdminToken], require("./routes/provincias"));
app.use("/api/localidades", [verifyAdminToken], require("./routes/localidades"));
app.use("/api/rubros", [verifyAdminToken], require("./routes/rubros"));
app.use("/api/categorias", [verifyAdminToken], require("./routes/categorias"));
app.use("/api/sucursales", [verifyToken], require("./routes/sucursales"));
app.use("/api/users", [verifyAdminToken], require("./routes/users"));
app.use("/api/auth", require("./routes/auth"));

// Static files
app.use(express.static(path.join(__dirname, "public")));

module.exports = app;
