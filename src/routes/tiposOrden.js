const { Router } = require("express");
const router = Router();

const {
  createTipoOrden,
  getTipoOrden,
  getTiposOrden,
  deleteTipoOrden,
  updateTipoOrden,
} = require("../controllers/tiposOrden.controller");

router.route("/")
  .get(getTiposOrden)
  .post(createTipoOrden);
  
router.route("/:id")
  .get(getTipoOrden)
  .delete(deleteTipoOrden)
  .put(updateTipoOrden);


module.exports = router;
