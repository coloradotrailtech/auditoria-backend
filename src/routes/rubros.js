const { Router } = require("express");
const router = Router();

const {
  getRubros,
  createRubro,
  getRubro,
  deleteRubro,
  updateRubro,
} = require("../controllers/rubros.controller");

router.route("/").get(getRubros).post(createRubro);

router.route("/:id")
  .get(getRubro)
  .delete(deleteRubro)
  .put(updateRubro);

module.exports = router;
