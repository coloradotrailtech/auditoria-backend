const { Router } = require("express");
const router = Router();

const {
  getClientes,
  createCliente,
  getCliente,
  deleteCliente,
  updateCliente,
  deactivateCliente,
} = require("../controllers/clientes.controller");
const { clientes } = require("../constants/multer");

router.route("/").get(getClientes).post(createCliente);

router
  .route("/:id")
  .get(getCliente)
  .delete(deleteCliente)
  .put(updateCliente)
  .patch(deactivateCliente);

module.exports = router;
