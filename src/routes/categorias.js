const { Router } = require("express");
const router = Router();

const {
  getCategoriasAfip,
  createCategoriaAfip,
  getCategoriaAfip,
  deleteCategoriaAfip,
  updateCategoriaAfip,
} = require("../controllers/categoriasAfip.controller");

router.route("/").get(getCategoriasAfip).post(createCategoriaAfip);

router
  .route("/:id")
  .get(getCategoriaAfip)
  .delete(deleteCategoriaAfip)
  .put(updateCategoriaAfip);

module.exports = router;
