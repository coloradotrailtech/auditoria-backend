const { Router } = require("express");
const router = Router();

const {

  getOrdenes,
  createOrden,
  getOrden,
  deleteOrden,
  updateOrden,
} = require("../controllers/ordenes.controller");

router.route("/")
  .get(getOrdenes)
  .post(createOrden);

router.route("/:id")
  .get(getOrden)
  .delete(deleteOrden)
  .put(updateOrden);
  
module.exports = router;