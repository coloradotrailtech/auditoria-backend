const { Router } = require("express");
const router = Router();

const {
  createCapacitacion,
  deleteCapacitacion,
  getCapacitacion,
  getCapacitaciones,
  updateCapacitacion,
} = require("../controllers/capacitaciones.controller");

router.route("/").get(getCapacitaciones).post(createCapacitacion);

router.route("/:id").get(getCapacitacion).delete(deleteCapacitacion).put(updateCapacitacion);

module.exports = router;
