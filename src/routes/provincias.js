const { Router } = require("express");
const router = Router();

const {
  getProvincia,
  getProvincias,
} = require("../controllers/provincias.controller");

router.route("/").get(getProvincias);

router.route("/:id").get(getProvincia);

module.exports = router;
