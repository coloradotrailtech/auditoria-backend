const { Router } = require("express");
const router = Router();

const {
  getLocalidades,
  createLocalidad,
  getLocalidad,
  deleteLocalidad,
  updateLocalidad,
} = require("../controllers/localidades.controller");

router.route("/")
  .get(getLocalidades)
  .post(createLocalidad);

router.route("/:id")
  .get(getLocalidad)
  .delete(deleteLocalidad)
  .put(updateLocalidad);

module.exports = router;
