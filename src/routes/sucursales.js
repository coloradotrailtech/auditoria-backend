const { Router } = require("express");
const router = Router();

const {
  getSucursales,
  createSucursal,
  getSucursal,
  deleteSucursal,
  updateSucursal,
} = require("../controllers/sucursales.controller");

router.route("/").get(getSucursales).post(createSucursal);

router.route("/:id")
  .get(getSucursal)
  .delete(deleteSucursal)
  .put(updateSucursal);

module.exports = router;
