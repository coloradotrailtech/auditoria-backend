const { Router } = require("express");
const router = Router();

const { getPaises, getPais } = require("../controllers/paises.controller");

router.route("/").get(getPaises);

router.route("/:id").get(getPais);

module.exports = router;
