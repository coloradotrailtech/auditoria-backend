const { Router } = require("express");
const router = Router();

const {
  createEmpleado,
  getEmpleados,
  getEmpleado,
  deleteEmpleado,
  updateEmpleado,
  getEmpleadosCliente,
} = require("../controllers/empleados.controller");

router.route("/").get(getEmpleados).post(createEmpleado);

router.route("/:id")
  .get(getEmpleado)
  .delete(deleteEmpleado)
  .put(updateEmpleado);

router.route("/cliente/:id").get(getEmpleadosCliente);

module.exports = router;
