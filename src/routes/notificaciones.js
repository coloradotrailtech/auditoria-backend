const { Router } = require("express");
const router = Router();

const {
    getNotificaciones,
    createNotificacion,
    getNotificacion,
    updateNotificacion
} = require("../controllers/notificaciones.controller");

router.route("/")
    .get(getNotificaciones)
    //.post(createNotificacion);

module.exports = router;