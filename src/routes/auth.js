const { Router } = require("express");
const router = Router();

const { signIn, verifyUser } = require("../controllers/auth.controller");

router.route("/verify/:id").get(verifyUser);
router.route("/signin").post(signIn);

module.exports = router;
